#!/bin/bash
eval $(cat vm.config)
multipass launch --name ${vm_name} \
  --cpus 1 \
  --mem 512M \
  --disk 3G \
  --cloud-init ./vm.cloud-init.yaml

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
echo "${IP} ${vm_domain}" > hosts.config

